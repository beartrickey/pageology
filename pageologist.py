# Python
import getopt
import HTMLParser
import json
import multiprocessing
import os
import re
import signal
import sys
import traceback
import urlparse

# Third Party
from bs4 import BeautifulSoup
import redis
import requests


class Pageologist():


	def __init__(self, schema, host, allowed_hosts, concurrent_processes):
		
		self.schema = schema
		self.host = host
		self.allowed_hosts = allowed_hosts
		self.allowed_hosts.append(self.host)
		print self.allowed_hosts
		self.origin_url = self.make_full_url(schema, host)

		# Multithreading
		self.concurrent_processes = concurrent_processes
		self.parent_pid = os.getpid()

		# Redis initialization
		self.redis = redis.Redis(
			host="localhost",
			port=6379,
			db=5
		)
		self.redis.flushall()
		self.redis.set("num_processes", 1)

	
	def make_full_url(self, schema, host):

		return schema + "://" + host


	def scan_site(self):

		self.scan_page(self.origin_url, self.origin_url)


	def scan_page(self, referrer, url):

		# Skip pages that have already been scanned
		if self.redis.sismember("scanned_links", url):
			return

		# Memoize which pages we've already scanned
		self.redis.sadd("scanned_links", url)

		# Send request for the resource
		try:
			r = requests.get(url)
		except requests.exceptions.InvalidSchema:
			# TODO: Log errors in redis
			return
		except:
			# TODO: Log errors in redis
			return

		# Save to redis
		url_dict = {
			"referrer": referrer,
			"url": url,
			"status_code": r.status_code,
			"elapsed_time": r.elapsed.total_seconds(),
			"content_length": r.headers.get("content-length")
		}
		url_json = json.dumps(url_dict)
		self.redis.rpush("urls", url_json)
		
		# Update url with redirect information
		if url != r.url:
			url = r.url

		purl = urlparse.urlparse(url)
		host = purl.hostname
		self.redis.sadd("referenced_hosts", host)

		# Don't scan links that dont' have an allowed_hosts list
		if host not in self.allowed_hosts:
			return

		# Collect links on page
		links = []
		try:
			links = self.get_links(
				referrer=url,
				html_doc=r.content
			)
		except HTMLParser.HTMLParseError:
			# TODO: Log errors in redis
			return

		links = set(links)

		processes = []
		for link in links:

			# Fork process here if workers available
			if int(self.redis.get("num_processes")) < self.concurrent_processes:

				self.redis.incr("num_processes", amount=1)

				# We can make a new thread. Assign this task to child process
				p = multiprocessing.Process(
					name=url,
					target=self.scan_page,
					args=(url,link)
				)
				p.start()
				processes.append(p)

			else:
				# Won't make anymore threads. Parent will continue on
				self.scan_page(
					referrer=url,
					url=link
				)

		# Block until all child processes finish
		any_processes_remaining = True
		while any_processes_remaining:
			any_processes_remaining = any(
				map(
					lambda p: p.is_alive(),
					processes
				)
			)

		self.redis.decr("num_processes", amount=len(processes))


	def get_links(self, referrer, html_doc):

		soup = BeautifulSoup(html_doc, "html.parser")
		links = []

		for link in soup.find_all("a"):
			url = self.make_absolute_url(referrer, link.get("href"))
			links.append(url)

		for link in soup.find_all("img"):
			url = self.make_absolute_url(referrer, link.get("src"))
			links.append(url)

		for link in soup.find_all("link"):
			url = self.make_absolute_url(referrer, link.get("href"))
			links.append(url)

		return links


	def make_absolute_url(self, referrer, url):

		# TODO: look into using urlparse.urljoin

		# Skip empty links
		if not url:
			return referrer

		# Skip telephone number links
		if re.match(r"^tel\:.*$", url):
			return referrer
		
		# Skip email links
		if re.match(r"mailto\:.*$", url):
			return referrer

		# Construct absolute url
		# If url arg domain is different from referrer arg domain, the url arg domain will be used
		# Ex from docs:
		# >>> urljoin('http://www.cwi.nl/%7Eguido/Python.html', '//www.python.org/%7Eguido')
		# >>> 'http://www.python.org/%7Eguido'
		url = urlparse.urljoin(referrer, url)

		return url


	def analyze_urls(self):

		url_list = self.redis.lrange("urls", 0, -1)

		master_url_list = map(
			lambda z: json.loads(z),
			url_list
		)

		filtered_url_list = filter(
			lambda z: z.get("status_code") == 404,
			master_url_list
		)

		for url in master_url_list:
			referrer = url.get("referrer").replace("qa-wac", "www")
			the_url = url.get("url").replace("qa-wac", "www")
			purl = urlparse.urlparse(the_url)
# 			resource = purl.path.replace(purl.hostname, "")
# 			mag_5_author_url = urlparse.urljoin("http://magnolia-5-author-fe-1.internal.atlassian.com:8090/magnolia-author/", resource)
# 			mag_4_author_url = urlparse.urljoin("https://wac-author.internal.atlassian.com/magnoliaAuthor/", resource)
			print "referrer:", referrer
			print "url: ", the_url
			print "status-code: ", url.get("status_code")
			print "elapsed-time: ", url.get("elapsed_time")
			print "content-length: ", url.get("content_length")
# 			print "mag 5: ", mag_5_author_url
# 			print "mag 4: ", mag_4_author_url
			print " "

		print "Largest content: ", max(
			map(
				lambda z: z.get("content_length"), 
				master_url_list
			)
		)

		print "Longest elapsed time: ", max(
			map(
				lambda z: z.get("elapsed_time"),
				master_url_list
			)
		)

		print "Links checked: ", len(master_url_list)


def main(argv):
	
	schema = ""
	host = ""
	allowed_hosts = []
	concurrent_processes = 1
	help_text = "usage: pageology.py --schema <schema> --host <hostname> --allowed-host <hostname>"
			
	try:
		opts, args = getopt.getopt(argv, "hS:H:A:C:", ("help", "schema=", "host=", "allowed-host=", "concurrent-processes="))
	except getopt.GetoptError:
		print help_text
		sys.exit(2)

	for opt, arg in opts:
		if opt == "-h":
			print help_text
			sys.exit()
		elif opt in ("-H", "--host"):
			host = arg
		elif opt in ("-A", "--allowed-host"):
			allowed_hosts = arg.split(" ")
		elif opt in ("-S", "--schema"):
			schema = arg
		elif opt in ("-C", "--concurrent-processes"):
			concurrent_processes = int(arg)
		
	# Make sure args were set
	if schema == "" or host == "":
		print help_text
		sys.exit(2)

	print "Schema: ", schema
	print "Host: ", host
	print "Allowed Hosts: ", allowed_hosts

	pageologist = Pageologist(
		schema=schema,
		host=host,
		allowed_hosts=allowed_hosts,
		concurrent_processes=concurrent_processes
	)

	pageologist.scan_site()
	pageologist.analyze_urls()


if __name__ == "__main__":
	main(sys.argv[1:])

